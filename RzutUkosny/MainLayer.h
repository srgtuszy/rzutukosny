//
//  MainLayer.h
//  RzutUkosny
//
//  Created by Michal Tuszynski on 6/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "chipmunk.h"

typedef enum {
    MovementStateIdle = 0,
    MovementStateInProgress = 1
    
} MovementState;

@interface MainLayer : CCLayer 

/**
 * Indicates if the movement is currently in progress or there
 * is not any movement taking place
 */
@property (assign, nonatomic, readonly) MovementState state;

/**
 * Creates and returns an initialized CCScene instance with MainLayer as a child
 */
+(id)scene;

/**
 * Creates and adds ball shape and body into current space
 */
-(void)createBall;

/**
 * Creates and initializes sliders for movement speed and angle along with helper 
 * labels which display information about currently selected value on label
 */
-(void)createSliders;

/**
 * Stops current movement and places the ball on it's initial position. This method is intended
 * to be called after a specific interval of time after movement started to prevent the user from waiting forever
 * until the ball hits the left wall
 */
-(void)resetMovement;

@end
