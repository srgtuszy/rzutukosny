//
//  MainLayer.m
//  RzutUkosny
//
//  Created by Michal Tuszynski on 6/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MainLayer.h"

#define SLIDER_WIDTH 200.0f
#define SLIDER_HEIGHT 20.0f

#define BUTTON_WIDTH  70.0f
#define BUTTON_HEIGHT 50.0f 

#define WALL_COLLISION_ID 666
#define BALL_COLLISION_ID 667

static CGPoint INITIAL_BALL_POSITION = {10.0f, 10.0f};

@interface MainLayer()

@property (assign, nonatomic) cpBody *ballBody;
@property (assign, nonatomic) cpSpace *space;
@property (assign, nonatomic) CCLabelTTF *speedLabel;
@property (assign, nonatomic) CCLabelTTF *lengthLabel;
@property (retain, nonatomic) UISlider *speedSlider;
@property (retain, nonatomic) UISlider *angleSlider;
@property (retain, nonatomic) UILabel *selectedSpeedLabel;
@property (retain, nonatomic) UILabel *selectedAngleLabel;

@end

static void eachShape(void *ptr, void* unused) {
	cpShape *shape = (cpShape*) ptr;
	CCSprite *sprite = shape->data;
	if( sprite ) {
		cpBody *body = shape->body;
				
		[sprite setPosition: body->p];
		[sprite setRotation: (float) CC_RADIANS_TO_DEGREES( -body->a )];
	}
}

static cpBool wallCollisionHandler(cpArbiter *arb, cpSpace *space, void *userData) {
    
    MainLayer *mainLayer = (MainLayer *)userData;
    [mainLayer resetMovement];
    
    return true;
}

@implementation MainLayer

@synthesize ballBody            = _ballBody;
@synthesize space               = _space;
@synthesize speedLabel          = _speedLabel;
@synthesize lengthLabel         = _lengthLabel;
@synthesize speedSlider         = _speedSlider;
@synthesize angleSlider         = _angleSlider;
@synthesize selectedSpeedLabel  = _selectedSpeedLabel;
@synthesize selectedAngleLabel  = _selectedAngleLabel;
@synthesize state               = _state;

#pragma mark - Setup

+(id)scene {
    
    CCScene *scene = [CCScene node];
    MainLayer *layer = [MainLayer node];
    [scene addChild:layer];
    
    return scene;
}

-(id)init {
    self = [super init];
    
    if (self) {
        
        //Setup chipmunk physics engine and 
        [[CCDirector sharedDirector] setDisplayFPS:NO];
        
        _state = MovementStateIdle;
        CGSize wins = [[CCDirector sharedDirector] winSize];
		cpInitChipmunk();
		
		cpBody *staticBody = cpBodyNew(INFINITY, INFINITY);
		_space = cpSpaceNew();
		cpSpaceResizeStaticHash(_space, 400.0f, 40);
		cpSpaceResizeActiveHash(_space, 100, 600);
		
		_space->gravity = ccp(-5, -50);
		_space->elasticIterations = _space->iterations;
		
		cpShape *shape;
		
		// bottom
		shape = cpSegmentShapeNew(staticBody, ccp(0,0), ccp(wins.width,0), 0.0f);
		shape->e = 1.0f; shape->u = 1.0f;
		cpSpaceAddStaticShape(_space, shape);
		
		// top
		shape = cpSegmentShapeNew(staticBody, ccp(0,wins.height), ccp(wins.width,wins.height), 0.0f);
		shape->e = 1.0f; shape->u = 1.0f;
		cpSpaceAddStaticShape(_space, shape);
		
		// left
		shape = cpSegmentShapeNew(staticBody, ccp(0,0), ccp(0,wins.height), 0.0f);
		shape->e = 1.0f; shape->u = 1.0f;
		cpSpaceAddStaticShape(_space, shape);
		
		// right
		shape = cpSegmentShapeNew(staticBody, ccp(wins.width,0), ccp(wins.width,wins.height), 0.0f);
		shape->e = 1.0f; shape->u = 1.0f;
        shape->collision_type = WALL_COLLISION_ID;
		cpSpaceAddStaticShape(_space, shape);
        
        cpSpaceAddCollisionHandler(self.space, WALL_COLLISION_ID, BALL_COLLISION_ID, &wallCollisionHandler, NULL, NULL, NULL, self);
        
        self.speedLabel = [CCLabelTTF labelWithString:@"0" fontName:@"Helvetica" fontSize:11.0f];
        [self addChild:self.speedLabel];
        
        [self schedule:@selector(tick:)];
        
        [self createBall];
        [self createSliders];
    }
    
    return self;
}

-(void)draw {
    
    //Draw a circle around the ball, because ball has no sprite along with it and will be
    //invisible if we won't draw the line
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(0.5);
    if(self.ballBody) ccDrawCircle(self.ballBody->p, 12.0f, 0, 10, YES);
}

-(void)tick:(ccTime)delta {
    
    int steps = 2;
	CGFloat dt = delta/(CGFloat)steps;
	
	for(int i=0; i<steps; i++){
		cpSpaceStep(_space, dt);
	}
	cpSpaceHashEach(_space->activeShapes, &eachShape, nil);
	cpSpaceHashEach(_space->staticShapes, &eachShape, nil);
    
    if (self.ballBody) {
        
        CGPoint newLabelPosition = CGPointMake(self.ballBody->p.x, self.ballBody->p.y + 30.0f);
        [self.speedLabel setPosition:newLabelPosition];
        [self.speedLabel setString:[NSString stringWithFormat:@"vx: %d\n vy: %d", (int)self.ballBody->v.x, (int)self.ballBody->v.y]];
    }
}

-(void)dealloc {
    cpSpaceDestroy(self.space); _space = NULL;
    [_speedLabel release]; _speedLabel = nil;
    [_angleSlider release]; _angleSlider = nil;
    
    [super dealloc];
}

#pragma mark - Public methods

-(void)createBall {
    
    self.ballBody = cpBodyNew(cpMomentForCircle(12.0f, 10.0f, 0, cpvzero), 12.0f);
    self.ballBody->p = INITIAL_BALL_POSITION;
    cpSpaceAddBody(self.space, self.ballBody);
    
    cpShape *ballShape = cpCircleShapeNew(self.ballBody, 12.0f, cpvzero);
    ballShape->e = 0.5f; ballShape->u = 0.5f;
    ballShape->collision_type = BALL_COLLISION_ID;
    cpSpaceAddShape(self.space, ballShape);
}

-(void)resetMovement {
    
    //No need to reset the movement if there is no movement going on
    if(_state == MovementStateIdle) return ;
    
    //We don't want this to be called multiple times
    [self unschedule:@selector(resetMovement)];
    
    //Reset all forces on the ball and place it on it's initial position
    self.ballBody->p = INITIAL_BALL_POSITION;
    self.ballBody->a = 0;
    self.ballBody->v = cpvzero;
    cpBodyResetForces(self.ballBody);
    _state = MovementStateIdle;
}

-(void)createSliders {
    
    UIView *glView = [[CCDirector sharedDirector] openGLView];
    
    CGRect speedSliderFrame = CGRectMake(0, 0, SLIDER_WIDTH, SLIDER_HEIGHT);
    CGRect angleSliderFrame = CGRectMake(0, SLIDER_HEIGHT + 5.0f, SLIDER_WIDTH, SLIDER_HEIGHT);
    CGRect buttonFrame = CGRectMake(glView.bounds.size.width - BUTTON_WIDTH, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
    CGRect speedLabelFrame = CGRectMake(speedSliderFrame.origin.x + speedSliderFrame.size.width, 
                                        speedLabelFrame.origin.y, 
                                        100.0f, 
                                        20.0f);
    CGRect angleLabelFrame = CGRectMake(angleSliderFrame.origin.x + angleSliderFrame.size.width, 
                                        angleSliderFrame.origin.y, 
                                        100.0f, 
                                        20.0f);
    
    self.speedSlider = [[[UISlider alloc] initWithFrame:speedSliderFrame] autorelease];
    [self.speedSlider setContinuous:YES];
    [self.speedSlider setMinimumValue:20.0f];
    [self.speedSlider setMaximumValue:100.0f];
    [self.speedSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.speedSlider setValue:20.0f];
    
    self.selectedSpeedLabel = [[[UILabel alloc] initWithFrame:speedLabelFrame] autorelease];
    [self.selectedSpeedLabel setBackgroundColor:[UIColor clearColor]];
    [self.selectedSpeedLabel setTextColor:[UIColor whiteColor]];
    [self.selectedSpeedLabel setText:@"20 (speed)"];
    
    self.angleSlider = [[[UISlider alloc] initWithFrame:angleSliderFrame] autorelease];
    [self.angleSlider setContinuous:YES];
    [self.angleSlider setMinimumValue:45.0f];
    [self.angleSlider setMaximumValue:90.0f];
    [self.angleSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.speedSlider setValue:45.0f];
    
    self.selectedAngleLabel = [[[UILabel alloc] initWithFrame:angleLabelFrame] autorelease];
    [self.selectedAngleLabel setBackgroundColor:[UIColor clearColor]];
    [self.selectedAngleLabel setTextColor:[UIColor whiteColor]];
    [self.selectedAngleLabel setText:@"45 (angle)"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTitle:@"Start" forState:UIControlStateNormal];
    [button setFrame:buttonFrame];
    [button addTarget:self action:@selector(beginMovement:) forControlEvents:UIControlEventTouchUpInside];
        
    [glView addSubview:self.speedSlider];
    [glView addSubview:self.angleSlider];
    [glView addSubview:self.selectedAngleLabel];
    [glView addSubview:self.selectedSpeedLabel];
    [glView addSubview:button];
}

#pragma mark Sliders/Buttons callbacks

-(void)beginMovement:(UIButton *)sender {
    
    //Begin movement only if there is no movement going on
    if (_state == MovementStateIdle) {
        
        _state = MovementStateInProgress;
        
        float angle = CC_DEGREES_TO_RADIANS([self.angleSlider value]);
        float speed = [self.speedSlider value];
       
        //Calculate the speed in both x and y axis and create a vector out of it
        //The values will be small, so in order for this to actually move the ball, we
        //need to make it a bit stronger
        float dx = cosf(angle) * speed * 1000;
        float dy = sinf(angle) * speed * 1000;
        
        //Apply the speed vectory to the body
        cpVect impulseVect = cpv(dx, dy);
        cpBodyApplyImpulse(self.ballBody, impulseVect, cpvzero);
        
        //The movement will end once the ball hits the left wall, but if it doesn't
        //we want it to end after 10 seconds
        [self scheduleOnce:@selector(resetMovement) delay:10.0f];
    }
}

-(void)sliderValueChanged:(UISlider *)sender {
    
    NSString *content = nil;
    
    if ([sender isEqual:self.angleSlider]) {
        
        content = [NSString stringWithFormat:@"%d (angle)", (int)[self.angleSlider value]];
        [self.selectedAngleLabel setText:content];
    }
    else if ([sender isEqual:self.speedSlider]) {
        
        content = [NSString stringWithFormat:@"%d (speed)", (int)[self.speedSlider value]];
        [self.selectedSpeedLabel setText:content];
    }
}

@end
